# Solution to Kiwi.com ml-weekend entry task

## Task assignment
https://github.com/kiwicom/mlweekend

## Short description

* API returns y values for our x.
* Get data from API (do more measurements)
* Find the formula f(x) = y

## Solution

* pandas for handling the data
* seaborn for visualization
* numpy for operations with ndarrays
* numpy.polyfit for finding the formula coefficients

## Some comments
Since I assume the formula was written by hand and the author did not bother to specify every coefficient with 5 and more digit precision and since the rounded coefficients visually provide identical results as average of 10 measurements from API, I rounded the coefficients so the formula is "nicer".

## Resulting formula
I assume the formula is f(x) = x<sup>4</sup> - 5x<sup>2</sup> + 5x - 6

## Graph
![Formula graph](graph.png)

